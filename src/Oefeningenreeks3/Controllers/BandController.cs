﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Oefeningenreeks3.Models;

namespace Oefeningenreeks3.Controllers
{
    public class BandController : Controller
    {
        private List<Band> bands;
        public BandController()
        {
            bands = BandInitializer.StartUp();
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Lijst()
        {
            return View(bands);
        }

        public IActionResult LijstMetLeden()
        {
            return View(bands);
        }

        [HttpGet]
        public IActionResult Maak(string bandnaam, int bandjaar)
        {
            return View(new Band() { Naam=bandnaam, Jaar=bandjaar });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Maak(Band band)
        {
            if (ModelState.IsValid) {
                bands.Add(new Band() { Naam=band.Naam, Jaar=band.Jaar });
                return View("Lijst", bands);
            }
            return View(band);
        }

        public IActionResult JSLijst()
        {
            return Json(bands);
        }
    }
}
